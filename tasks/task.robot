*** Settings ***
Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    ../libraries/MyLibrary.py
Resource    ../resources/my_keywords.resource


*** Variables ***
${CAMUNDA_HOST}    ${EMPTY}


*** Tasks ***
Demo task
    Log hello world
    ${two}    Add One    1
    Should Be Equal As Integers    2    ${two}
This is a template project for simple for Robot Framework projects that are based on Camunda and plain Robot Framework. 
The this templates provides a basic kubernetes overlay for running the robot task as a cronjob.

It provides a basic directory structure:

- **k8s** : Kubernetes configuration
- **libraries** : Folder for local python keywords
- **resources** : Folder for local robot keywords
- **tasks** : Folder containing actual robot tasks
- **tests** : Folder for robot tests
- **variables** : Folder for variable files
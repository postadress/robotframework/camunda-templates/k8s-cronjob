from robot.api.deco import library, keyword

@library
class MyLibrary:

    @keyword
    def add_one(self, number: int) -> int:
        return number+1
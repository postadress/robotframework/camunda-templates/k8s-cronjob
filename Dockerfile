FROM python:3.9

WORKDIR /usr/src/app

COPY tasks/ ./tasks/
COPY libraries/ ./libraries/
COPY resources/ ./resources/
COPY variables/ ./variables/
COPY install/requirements.txt .

RUN mkdir logs

RUN python -m pip install -r requirements.txt

CMD ["robot", "--outputdir", "logs","-b", "debug.log", "-L","DEBUG","tasks/task.robot"]
